﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_puzzle3
{
    class Program
    {

        public static void Main()
        {
            string path = @"C:\New folder (2)\Text.txt";

            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.WriteLine("hello world");
                }

                using (StreamReader sr = new StreamReader(path))
                {
                   
                    char[] c = null;

                    while (sr.Peek() >= 0)
                    {
                        c = new char[7];
                        sr.Read(c, 0, c.Length);
                        
                        Console.WriteLine(c);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }
    }
}