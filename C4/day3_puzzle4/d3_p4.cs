﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_puzzle4
{
    class d3_p4
    {
       
			static int R = 5, C = 5;
			static bool island(int[,] V, int r,int c, bool[,] perimeter)
			{
				return (r >= 0) && (r < R) && (c >= 0) && (c < C) && (V[r, c] == 1 && !perimeter[r, c]);
			}
			static void search(int[,] v, int r,int c, bool[,] perimeter)
			{
				int[] rowMat = new int[] { -1, -1, -1,  0,  0,  1, 1, 1 };
				int[] colMat = new int[] { -1,  0,   1, -1, 1, -1, 0, 1 };

				perimeter[r, c] = true;

				for (int i = 0; i < 8; ++i)
					if (island(v, r + rowMat[i], c + colMat[i], perimeter))
						search(v, r + rowMat[i], c + colMat[i], perimeter);
			}
			static int totalIslands(int[,] v)
			{
				bool[,] perimeter = new bool[R, C];

				int count = 0;
				for (int i = 0; i < R; ++i)
					for (int j = 0; j < C; ++j)
						if (v[i, j] == 1 && !perimeter[i, j])
						{
							search(v, i, j, perimeter);
							++count;
						}

				return count;
			}

			public static void Main()
			{
				int[,] i = new int[,] { { 1, 0, 0, 0, 0 },
										{ 0, 0, 1, 1, 0 },
										{ 0, 1, 1, 0, 0 },
										{ 0, 0, 0, 0, 0 },
										{ 1, 1, 0, 0, 1 },
										{ 1, 1, 0, 0, 1 } };
				Console.WriteLine("total Number of islands is: " + totalIslands(i));
			}
		}
	}
    
