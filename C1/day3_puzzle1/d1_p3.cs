﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_puzzle1
{
 public class d3_p1
        {
            item head;
        public class item
        {
            public int data;
            public item link;

            public item(int adress)
            {
                data = adress;
                link = null;
            }
        }


        void find(item node)
        {
            while (node != null)
            {
                Console.Write(node.data + " ");
                node = node.link;
            }
        }



        item reverse(item current, item previous)
            {
               if (current.link == null)
                {
                    head = current;
                    current.link = previous;

                    return head;
                }

               item nextNode = current.link;
               current.link = previous;
                reverse(nextNode, current);
                return head;
            }
           
            public static void Main(String[] args)
            {
                d3_p1 list = new d3_p1();
                list.head = new item(1);
                list.head.link = new item(2);
                list.head.link.link = new item(3);
                list.head.link.link.link = new item(4);

                Console.WriteLine(" Linked list ");
                list.find(list.head);

                item result = list.reverse(list.head, null);
                Console.WriteLine("");
         
                Console.WriteLine("Reverse of  linked list ");
                list.find(result);
            }
        }

    }

