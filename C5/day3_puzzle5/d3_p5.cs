﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_puzzle5
{
    class d3_p5
    { 
            internal static void per(int n)
            {
                int tem = n, count = 0;
                
                while (tem > 0)
                {   
                    count++;
                    tem = tem / 10;
                }
                
                int[] num = new int[count];
                while (n > 0)
                {
                
                    num[count-- - 1] = n % 10;
                    n = n / 10;
                }
              
                while (permutations(num))
                {

                    for (int i = 0; i < num.Length; i++)
                    { 
                        Console.Write(num[i]);
                    } 
                    Console.Write("\n");
                }
            } 
            internal static bool permutations(int[] p)
            {
                for (int i = p.Length - 2; i >= 0; --i)
                {
                    if (p[i] < p[i + 1])
                    {
                        for (int j = p.Length - 1; ; --j)
                        {
                            if (p[j] > p[i])
                            {
                                
                                int t = p[i];
                                p[i] = p[j];
                                p[j] = t;
                                for (++i, j = p.Length - 1; i < j; ++i, --j)
                                {
                                    t = p[i];
                                    p[i] = p[j];
                                    p[j] = t;
                                }
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        public static void Main(string[] args)
        {

            int n = 123;
            per(n);
        }
    }

    }


