﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3_puzzle2
{ 
		public class node
        {
			public char value;
			public node Lnode, Rnode;
			public node(char K)
            {
				value = K;
				Lnode = null;
				Rnode = null;
            }
         };

    class d3_p2
    {
          public static node lastnode(node head)
        {
            node temp = null;
            if (head == null)
                return null;

         
            Queue<node> q = new Queue<node>();
            q.Enqueue(head);

            
            while (q.Count != 0)
            {
                temp = q.Peek();
                q.Dequeue();
                if (temp.Lnode != null)
                    q.Enqueue(temp.Lnode);
                if (temp.Rnode != null)
                    q.Enqueue(temp.Rnode);
            }
            return temp;
        }

        
        public static void Main(String[] args)
        {

            node root = new node('a');
            root.Lnode = new node('b');
            root.Rnode = new node('c');
            root.Lnode.Lnode = new node('d');

            Console.WriteLine("the deepest node is");
            node deepNode = lastnode(root);
            Console.WriteLine(deepNode.value);
        }

    }
    }
